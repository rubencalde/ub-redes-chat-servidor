package Modelo;


import java.util.HashMap;
import java.util.Map;
import sun.org.mozilla.javascript.json.JsonParser;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alex
 */
public class Mensaje {

    private HashMap<String, String> data;
    private int id;
    private String msg;

    public Mensaje(int id, String msg) {
        this.id = id;
        this.msg = msg;
        
        this.data =  new HashMap<String, String>();
    }

    public int get_id() {
        return this.id;
    }

    public String get_msg() {
        return this.msg;
    }
    @Override
    public String toString(){
        return this.msg;
    }
    
    public String get_json(){
        
        data.put("mensaje", this.msg);
        data.put("user_id", String.valueOf(this.id));
        
        return "";
    }
}
