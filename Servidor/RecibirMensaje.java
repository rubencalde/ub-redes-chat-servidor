package Servidor;


import Modelo.Mensaje;
import java.io.InputStream;
import java.util.Observable;
import java.util.Scanner;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alex
 */
public class RecibirMensaje extends Observable implements Runnable{
    private InputStream entrada;
    private Thread hilo;
    private int id;
    public RecibirMensaje(InputStream entrada, int id){
        this.entrada = entrada;
        this.id = id;
        this.hilo = new Thread(this);
        this.hilo.start();
    }

    @Override
    public void run() {
        Scanner in = new Scanner(entrada);
        while(in.hasNextLine()){
            String line = in.nextLine();
            setChanged();
            notifyObservers(new Mensaje(this.id, line));
        }   
    }
}
