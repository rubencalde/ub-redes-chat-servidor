package Servidor;

//package serversocket;
import Modelo.Mensaje;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author manel
 */
public final class Server implements Observer {

    private ArrayList<Usuario> usuarios;
    private ArrayList<Mensaje> mensajes;
    int id;

    public Server() {

        this.usuarios = new ArrayList<Usuario>();
        this.mensajes = new ArrayList<Mensaje>();
        this.id = 1;

        welcome();

        try {
            ServerSocket server = new ServerSocket(8189);
            while (true) {
                Socket socket = server.accept();
                info_conexion();
                Usuario usr = new Usuario(socket, this.id);
                usr.rm.addObserver(this);
                this.usuarios.add(usr);
                this.id++;
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void welcome() {
        System.out.println("UB - Redes: Chat cliente servidor.");
        System.out.println("\t· Puerto de trabajo: 8189");
        System.out.println("\t· Clientes conectados: " + this.usuarios.size());
    }

    public void info_conexion() {
        System.out.println("-> Aceptada nueva conexión.");
        System.out.println("-> Clientes conectados:" + this.usuarios.size());
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new Server();
    }

    @Override
    public void update(Observable objeto, Object mensaje) {
        Mensaje msg = (Mensaje) mensaje;
        if (objeto instanceof RecibirMensaje) {
            mensaje_recibido(msg);
            enviar_mensaje(objeto, msg);
        }
    }

    private void mensaje_recibido(Mensaje msg) {
        this.mensajes.add(msg);
        System.out.println(msg);
    }

    private void enviar_mensaje(Observable objeto, Mensaje msg) {
        for (Usuario u : this.usuarios) {
            u.enviar_mensaje(msg);
        }
    }
}

class Usuario {

    private Socket socket;
    private int id;
    
    private InputStream entrada;
    private OutputStream salida;
    private PrintWriter out;
    
    RecibirMensaje rm;

    public Usuario(Socket skt, int id) {
        this.socket = skt;
        this.id = id;
        config();

    }

    public void config() {
        try {
            //Inicializo la entrada.
            this.entrada = socket.getInputStream();
            this.rm = new RecibirMensaje(entrada, this.id);

        } catch (IOException ex) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            this.salida = socket.getOutputStream();
            this.out = new PrintWriter(salida, true);
        } catch (IOException ex) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void enviar_mensaje(Mensaje msg) {
         this.out.println(msg.get_msg());
    }
}